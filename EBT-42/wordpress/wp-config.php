<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db_wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'sBGoSPCrL<#W82p?]GQxqZ.E1>t0=Pr&4V>U-_cAu6HTEM0zv`WlG;IHM92Ee:Jn');
define('SECURE_AUTH_KEY',  'YsdUqB]&`( +gleQ?HQMxVY?mOryAo}1FlDg3Qik@{d$gfXpuqA-)dgbYX%s1:Ip');
define('LOGGED_IN_KEY',    '7tbn<kL=,-iDv+KK!i]hUV>xLfen-0AGZ0#PLOph7=-1M>iLi?7q!l5r+:Vc%2)%');
define('NONCE_KEY',        '>4cn~X)7D;/zmhg`v8ibwfY~ TS;+E~>i[tj]nIBx9WI9Yh_@tGf>,Rq`r]f]eap');
define('AUTH_SALT',        'cJC>wmhoo_hwvM0NxzgtI`zZX5bbxwBBUJr?yw$29L(rKR*zM`tA`a%OC8hG3LYB');
define('SECURE_AUTH_SALT', 'ac(y&o%Z7sZ_QAN5sq+lZ9=2jq5gGyC)qw8/|4ES}KlByb)k@@]6hoK||.*WA^XE');
define('LOGGED_IN_SALT',   'r;[t:yT^nrW!^:}6e@scquD1%x>L^2)OgG*2({g?jue{OfLZ]Ie}!KNvmLVXkI^~');
define('NONCE_SALT',       'dkcB&4a-A1lQV4LEI0NDQ|d}},yLV}I2E3X7S;Y_)KypV(74n!zXQcmd[(d lC~n');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
