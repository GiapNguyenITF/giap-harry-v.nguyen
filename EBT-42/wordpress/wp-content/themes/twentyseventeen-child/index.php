<?php
  get_header();
?>

  <!-- body page -->
  <!-- start section 1 -->
  <!-- <?php
    $st_1_title_1 = new WP_Query([
        'post_type'=>'home_page',
        'name'=>'what-is'
    ]);
     $st_1_title_2 = new WP_Query([
        'post_type'=>'home_page',
        'name'=>'the-curseborn-saga'
    ]);
  ?> -->
  <div id="section-1">
    <div class="container">
      <div class="row">
        <div class="col-md-7">
          <div class="row">
            <div class="col-md-12">
              <h1 class="st-1-title" id="st-1-title-1"><?php echo $st_1_title_1->post->post_title; ?></h1>
              <h1 class="st-1-title" id="st-1-title-2"><?php echo $st_1_title_2->post->post_title; ?></h1>
              <hr id="st-1-hr">
            </div>
          </div>
          <div class="row">
            <div class="col-md-8">
              <?php
                $st_1_content = new WP_Query([
                    'post_type'=>'home_page',
                    'name'=> 'the-novella-series'
                ]);
              ?>
              <div id="section-1-content-1"><?php echo apply_filters('the_title',$st_1_content->post->post_title); ?></div>
              <div id="section-1-content-2"><?php echo apply_filters('the_content',$st_1_content->post->post_content); ?></div>
              <a id="button-readmore" href="">
                <img class="img-responsive" src="<?php bloginfo('stylesheet_directory') ?>/assets/img/button-readmore.png" alt="">
              </a>
            </div>
          </div>

        </div>
        <div class="col-md-5">
          <img id="img-circle" class="img-responsive" src="<?php bloginfo('stylesheet_directory') ?>/assets/img/Ellipse 7.png" alt="Eclipse">
        </div>
      </div>
    </div>
  </div>
  <!-- end section 1 -->

  <!-- section 2 -->
  <?php
    $st_2_title =new WP_Query([
        'post_type'=> 'home_page',
        'name'=> 'the-characters'
    ]);
  ?>
  <div class="section-2" id="section-2" style="background-image: url('<?php echo get_the_post_thumbnail_url(20,'') ?>');">
    <div class="container">
      <h1 id="st-2-title"><?php echo $st_2_title->post->post_title; ?></h1>
      <hr class="st-6-hr-1">
      <hr class="st-6-hr-2">

      <?php

        $st_2_content = new WP_Query([
            'post_type'=>'the_characters',
        ]);
        if($st_2_content->have_posts()){
            while($st_2_content->have_posts()){
                $st_2_content->the_post();
                $img = get_the_post_thumbnail_url($st_1_content->post->ID);
            }
        }
      ?>
      <div class="section-2-slide">
      
        <?php
          for($i=0;$i<3;$i++){
            $query_st_2 =new WP_Query([
            'post_type'=>'the_characters',
          ]);
          if($query_st_2->have_posts()){
            while($query_st_2->have_posts()){
              $query_st_2->the_post();
              $img = get_the_post_thumbnail_url($query_st_2->post->ID);
              ?>
              <div class="col-md-3">
                <img src="<?php echo $img; ?>" alt="">
                <h3><?php the_title(); ?></h3>
                <p class="introduce-character"><?php the_content();?></p>
              </div>
              <?php
            }
          }
          }
        ?>
        <!--  -->
      
      </div>
    </div>
  </div>
  <!-- end section 2 -->

  <!-- start section 3 -->
  <?php
    $section_3_post_1 = new WP_Query([
      'post_type'=>'the_crew',
      'name'=>'the-crew'
    ]);
    $section_3_post_2 = new WP_Query([
      'post_type'=>'the_crew',
      'name'=>'the-four-lords'
    ]);
  ?>
  <div class="section-3" id="section-3">
    <div class="container">
      <h1 id="section-3-title">
        <?php echo $section_3_post_1->post->post_title ;?>
      </h1>
      <hr id="section-3-hr-1">
      <hr id="section-3-hr-2">
      <div><?php echo $section_3_post_1->post->post_content; ?></div>
      <div id="section-3-post-2"><?php echo $section_3_post_2->post->post_title ?></div>
      <div id="section-3-post-3"><?php echo $section_3_post_2->post->post_content ?></div>
      <a id="section-3-btn" href="#">
        <img class="img-responsive" id="section-3-img" src="<?php bloginfo('stylesheet_directory')?>/assets/img/button-readmore.png" alt="">
      </a>
    </div>
  </div>
  <!-- end section 3 -->

  <!-- start section 4 -->
  <div id="section-4" style="background-image: url('<?php echo get_the_post_thumbnail_url(27,'') ?>');">
    <div id="section-4-slide">
      <div class="container">
        <?php 
          $section_4_title = new WP_Query([
            'post_type'=>'home_page',
            'name'=> 'the-novellas'
          ]);
        ?>
        <h3 id="st-3-title"><?php echo $section_4_title->post->post_title;?></h3>
        <hr class="st-6-hr-1">
        <hr class="st-6-hr-2">

        <div class="section-4-slide">
          <?php
           for($i=0;$i<3;$i++){
              $section_4_content = new WP_Query([
              'post_type'=>'the_novellas',

            ]);

            if($section_4_content->have_posts()){
              while($section_4_content->have_posts()){
                $section_4_content->the_post();
                ?>
                <div class="col-md-3">
                   <div class="st-4-item">
                     <span><?php the_title(); ?></span>
                   </div>
                </div>
                <?php
              }
            }
           }
          ?>
         
        </div>
      </div>
    </div>
  </div>
  <!-- end section 4 -->

  <!-- start section 5 -->
  <?php
    $section_5_post = new WP_Query([
      'post_type'=> 'home_page',
      'name'=>'see-our-world'
    ]);
  ?>
  <div id="section-5">
    <div class="container">
      <div class="row">
        <div class="">
          <h1 class="st-5-title"><?php echo $section_5_post->post->post_title; ?></h1>
        </div>
        <hr id="st-5-hr-1">
        <hr id="st-5-hr-2">
        <div id ="st-5-content"><?php echo $section_5_post->post->post_content; ?></div>
      </div>
      
      <div class="row filter">
        <div class="col-md-4">
          <h3 id="st-5-h3-1">Filter by type</h3>
        </div>
        <div class="col-md-6 col-md-offset-2">
          <h3 id="st-5-h3-2">
            <a id="btn-character">The Characters</a>
            <span>|
              <a id="btn-artwork">Conceptual Artwork</a>
            </span>
          </h3>
        </div>
      </div>

      <!--start grid  -->
      <div class="grid">
        <?php
          $section_5_artwork = new WP_Query([
            'post_type'=>'artwork'
          ]);
          if($section_5_artwork->have_posts()){
            while($section_5_artwork->have_posts()){
              $section_5_artwork->the_post();
              ?>
              <div class="element-item artwork col-md-3 img-responsive">
                 <a data-fancybox="artwork" href="<?php the_post_thumbnail_url(); ?>">
                     <img class="img-responsive" src="<?php the_post_thumbnail_url(); ?>" alt="">
                 </a>
             </div>
            <?php
            }
          }
        ?>
        <?php
          $section_5_characters = new WP_Query([
            'post_type'=>'characters'
          ]);
          if($section_5_characters->have_posts()){
            while($section_5_characters->have_posts()){
              $section_5_characters->the_post();
              ?>
              <div class="element-item character col-md-3">
                <a data-fancybox="character" href="<?php the_post_thumbnail_url(); ?>">
                  <img class="img-responsive" src="<?php the_post_thumbnail_url(); ?>" alt="">
                </a>
              </div>
            <?php
            }
          }
        ?>

      </div>
      <!-- end grid -->
      <div class="row">

      </div>
    </div>
  </div>
  <!-- end section 5 -->

  <!-- start section 6 -->
  <?php
    $section_6_post = new WP_Query([
      'post_type'=> 'home_page',
      'name'=> 'bazaar'
    ]);
  ?>
  <div id="section-6" style="background-image: url('<?php echo get_the_post_thumbnail_url(60,'') ?>');">
    <div class="container">
      <h3 id="section-6-title"><?php echo $section_6_post->post->post_title; ?></h3>
      <hr class="st-6-hr-1">
      <hr class="st-6-hr-2">
      <div class="col-md-10 col-md-offset-1">
        <div id="st-6-content">
          <?php echo $section_6_post->post->post_content; ?>
        </div>
      </div>
    </div>
  </div>
  <!-- end section 6 -->

  <!-- start section 7 -->
  <?php
    $st_7_post = new WP_Query([
      'post_type'=> 'home_page',
      'name'=> 'feel-free-to-contact-us'
    ]);
  ?>
  <div id="section-7">
    <div class="container">
      <h3 id="st-7-h3">
        <?php echo $st_7_post->post->post_title; ?>
      </h3>
      <hr class="st-6-hr-1">
      <hr class="st-6-hr-2">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div id="st-7-p-1">
            <?php echo $st_7_post->post->post_content; ?>
         </div>
      <?php
                  gravity_form( 1, false, false, false, '', true);
              ?>
<!--          
          <div class="row">
         
            <div class="col-md-6">
              <p id="label-name">Name
                <span>&#42;</span>
              </p>
              <input id="input-name" id="name" class="form-control" type="text">
            </div>
            <div class="col-md-6">
              <p id="label-email">Email Address
                <span>&#42;</span>
              </p>
              <input id="input-email" id="" class="form-control" type="text">
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <p id="label-message">Message
                <span>&#42;</span>
              </p>
              <textarea class="form-control" rows="5" id="comment"></textarea>
              <input class="btn center-block" id="btn-send" type="submit" value="SEND">
            </div>
          </div>
        </div> -->

      </div>
    </div>
  </div>

  <!-- end section 7 -->

<?php
  get_footer();
?>
