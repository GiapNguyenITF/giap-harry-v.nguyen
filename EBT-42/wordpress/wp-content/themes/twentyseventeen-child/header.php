
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>

<body>
   <?php

        $query = array(
            "post_type"=>"home_page",
            "name"=>"night-and-day"
        );
        
        $data = new WP_Query($query);
        
        ?>
	<header style="background-image: url('<?php echo get_the_post_thumbnail_url(4,'') ?>');">
    <!-- nav start -->
    <nav class="navbar navbar-fixed-top">
      <div class="container-fluid">
        <!-- row -->
        <div class="row navbar-top">
          <div class="col-md-3 col-md-offset-1">
            <!-- <img class="img-responsive" id="logo" src="public/img/logo.png" alt="Logo"> -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <!-- "Menu" -->
                Menu
                <i class="fa fa-bars" aria-hidden="true"></i>
              </button>
              <a class="navbar-brand" href="#">
                <img class="img-responsive" id="logo" src="<?php bloginfo('stylesheet_directory');?>/assets/img/logo.png" alt="Logo">
              </a>
            </div>
          </div>
          <div class="col-md-7 col-md-offset-1">

          <?php
                                          if (has_nav_menu('main-nav')) :
                                            wp_nav_menu([
                                              'theme_location' => 'main-nav',
                                              'menu_class' => 'nav',
                                              'container' => 'div',
                                              'container_class' => 'navbar-collapse collapse',
                                              'container_id' => 'myNavbar',
                                              'menu_class' => 'nav navbar-nav',
                                              
                                        ]);
                                          endif;
                                          ?>
            <!-- <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li>
                  <a id="home" class="active">
                    <span id="home-active">HOME</span>
                  </a>
                </li>
                <li>
                  <a id="info"><span id="info-active">INFO</span></a>
                </li>
                <li>
                  <a id="theBookSeries"><span id="book-active">THE BOOK SERIES</span></a>
                </li>
                <li>
                  <a id="seeOurWorld"><span id="sow-active">SEE OUR WORLD</span></a>
                </li>
                <li>
                  <a id="bazaar"><span id="bazaar-active">BAZAAR</span></a>
                </li>
                <li>
                  <a id="theCrew"><span id="crew-active">THE CREW</span></a>
                </li>

              </ul>
            </div> -->
          </div>
        </div>
        <!-- end row -->
      </div>
    </nav>
    <!-- nav end -->
   
    <div class="container">
      <div id="header-body">
        <h1 id="header-title"><?php echo $data->post->post_title; ?></h1>
        <p id="header-content"><?php echo $data->post->post_content; ?></p>
      </div>
      <div id="scroll-down">
        <a href="">
          <img src="<?php bloginfo('stylesheet_directory');?>/assets/img/scroll-down.png" alt="Scroll down">
        </a>
      </div>
    </div>

  </header>
  <!-- end header -->

