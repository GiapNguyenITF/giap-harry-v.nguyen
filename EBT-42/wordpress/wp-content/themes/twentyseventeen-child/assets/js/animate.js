$(document).ready(function () {
    $('.section-2-slide').slick({
        autoplay: true,
        dots: true,
        arrows: false,
        slidesToShow: 4,
        slidesToScroll: 4,
        infinite: false,
        responsive: [{
            breakpoint: 1024,
            settings: {
                arrows: false,
                dots: true,
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }
        ]

    });

    $('.section-4-slide').slick({
        autoplay: true,
        dots: true,
        arrows: false,
        slidesToScroll: 4,
        slidesToShow: 4,
        infinite: false,
        responsive: [
            {
                breakpoint: 1280,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });

    // isotope
    $('#btn-artwork').on('click', function () {
        $('.grid').isotope({ filter: '.artwork' });
        $(this)
            .css('color', '#222222')
            .css('font-weight', '600');
        $('#btn-character')
            .css('color', '#6c6c6c')
            .css('font-weight', '200');
    });

    $('#btn-character').on('click', function () {
        $('.grid').isotope({ filter: '.character' });
        $(this)
            .css('color', '#222222')
            .css('font-weight', '600');
        $('#btn-artwork')
            .css('color', '#6c6c6c')
            .css('font-weight', '200');
    });

    // link navigation scroll

    $('#menu-item-86').click(function () {
        $('html, body').animate({ scrollTop: $('#section-1').offset().top }, 'slow');
    });
    $('#menu-item-87').click(function () {
        $('html, body').animate({ scrollTop: $('#section-2').offset().top }, 'slow');
    });
    $('#menu-item-88').click(function () {
        $('html, body').animate({ scrollTop: $('#section-5').offset().top }, 'slow');
    });
    $('#menu-item-89').click(function () {
        $('html, body').animate({ scrollTop: $('#section-6').offset().top }, 'slow');
    });
    $('#menu-item-90').click(function () {
        $('html, body').animate({ scrollTop: $('#section-7').offset().top }, 'slow');
    });


  

});
    






