
  <!-- footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-6 " >
         <div id="copy-right">
            <p id="p-1">ALL RIGHTS RESERVED. COPYRIGHT © 2015</p>
            <p id="p-2">THE CURSEBORN SAGA</p>
         </div>
        </div>
        <div class="col-md-6 ">
          <div id="social-network">
            <ul>
              <li>
                <a href="#">
                  <img src="<?php bloginfo('stylesheet_directory');?>/assets/img/dribbble.png" alt="">
                </a>
              </li>
              <li>
                <a href="#">
                  <img src="<?php bloginfo('stylesheet_directory');?>/assets/img/linkedin.png" alt="">
                </a>
              </li>
              <li>
                <a href="#">
                  <img src="<?php bloginfo('stylesheet_directory');?>/assets/img/google.png" alt="">
                </a>
              </li>
              <li>
                <a href="#">
                  <img src="<?php bloginfo('stylesheet_directory');?>/assets/img/twitter.png" alt="">
                </a>
              </li>
              <li>
                <a href="#">
                  <img src="<?php bloginfo('stylesheet_directory');?>/assets/img/facebook.png" alt="">
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>

<?php wp_footer(); ?>

</body>
</html>
