<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        //css 
        wp_enqueue_style('bootstrap.min',get_stylesheet_directory_uri().'/assets/css/bootstrap.min.css');
        wp_enqueue_style('style',get_stylesheet_directory_uri().'/assets/css/style.css');
        wp_enqueue_style('slick','https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css');
        wp_enqueue_style('slick_theme','https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css');
        wp_enqueue_style('fancybox','https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css');
        

        //js
        wp_enqueue_script('jquery_min',get_stylesheet_directory_uri().'/assets/js/jquery-3.2.1.min.js',array(),'',true);
        wp_enqueue_script('bootstrap_min',get_stylesheet_directory_uri().'/assets/js/bootstrap.min.js',array(),'',true);
        wp_enqueue_script('animate',get_stylesheet_directory_uri().'/assets/js/animate.js',array(),'',true);
        wp_enqueue_script('slick_min','https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js',array(),'',true);
        wp_enqueue_script('isotope_min','https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js',array(),'',true);
        wp_enqueue_script('fancybox_min','https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js',array(),'',true);
        wp_enqueue_script('fontawesome','https://use.fontawesome.com/a6499bd878.js',array(),'',true);
        

}
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 2 );

// // END ENQUEUE PARENT ACTION

register_nav_menus(
        array(
            'main-nav' => 'Main menu',
        )
);
